from pathlib import Path

from atpsync.match_features_to_pois import match_features_to_pois
from atpsync.update_features import update_features
from atpsync.update_pois import update_pois
from postgres_db import PostgresDB

db = PostgresDB()


def main():
    with db.get_cursor() as cursor:
        print(":: Cleaning database")
        cursor.execute("TRUNCATE found_features, generations restart identity")
        print(":: Updating POIs")
        update_pois(cursor)
        print(":: Updating features")
        update_features(cursor, Path("permanent_cache"), Path("debug"))
        print(":: Finding matches")
        match_features_to_pois(cursor)


if __name__ == "__main__":
    main()
