class PointOfInterest:
    type: str
    id: int
    class_: str
    subclass: str
    geom_x: float
    geom_y: float
    tags: dict[str, str]

    @classmethod
    def from_rs(cls, rs):
        i = cls()
        i.type = rs[0]
        i.id = rs[1]
        i.class_ = rs[2]
        i.subclass = rs[3]
        i.geom_x = rs[4]
        i.geom_y = rs[5]
        i.tags = rs[6]
        return i
