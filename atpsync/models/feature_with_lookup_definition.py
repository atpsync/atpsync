import json
from dataclasses import dataclass
from typing import Any, Optional


@dataclass
class FeatureWithLookup:
    id: str
    lookup_class: str
    lookup_subclass: Optional[str]
    lookup_name: Optional[str]
    lookup_brand: Optional[str]
    lookup_operator: Optional[str]
    lookup_radius: int
    geom_x: float
    geom_y: float
    properties: dict[str, Any]
    spider: str

    @staticmethod
    def get_template(include_cache: bool):
        template = "%s,%s,%s,%s,%s,%s,%s,ST_Point(%s,%s),%s,%s"
        if include_cache:
            template += ",%s,%s"
        return template

    def get_values(self, include_cache: bool):
        values = [json.dumps(value) if key == "properties" else value for key, value in self.__dict__.items()]
        if include_cache:
            values += [self.properties.get("brand:wikidata"), self.properties.get("ref")]
        return values

    @classmethod
    def get_rs_size(cls):
        return 11

    @classmethod
    def from_rs(cls, rs):
        return cls(*rs[0:cls.get_rs_size()])
