import json
from dataclasses import dataclass
from typing import Optional, Any

from .feature_with_lookup_definition import FeatureWithLookup
from .poi import PointOfInterest


@dataclass
class FoundFeatures:
    id: Optional[int]
    generation_id: int
    new_tags: dict[str, str]
    spiders: list[str]
    properties: list[dict[str, Any]]
    # feature: FeatureWithLookup
    poi: PointOfInterest

    # @staticmethod
    # def get_template():
    #     return f"%s,%s, {FeatureWithLookup.get_template(False)}, %s,%s,%s,%s,ST_Point(%s,%s,4326),%s"

    @staticmethod
    def get_template():
        return f"%s,%s,%s,%s::json[], %s,%s,%s,%s,ST_Point(%s,%s,4326),%s"

    # @staticmethod
    # def get_insert_fields():
    #     return """
    #     generation_id,
    #     new_tags,
    #     feature_id,
    #     feature_lookup_class,
    #     feature_lookup_subclass,
    #     feature_lookup_name,
    #     feature_lookup_brand,
    #     feature_lookup_operator,
    #     feature_lookup_radius,
    #     feature_geom,
    #     feature_properties,
    #     feature_scraper,
    #     poi_type,
    #     poi_id,
    #     poi_class,
    #     poi_subclass,
    #     poi_geom,
    #     poi_tags
    #     """

    @staticmethod
    def get_insert_fields():
        return """
        generation_id,
        new_tags,
        spiders,
        properties,
        poi_type,
        poi_id,
        poi_class,
        poi_subclass,
        poi_geom,
        poi_tags
        """

    # def get_values(self):
    #     return (
    #         self.generation_id,
    #         json.dumps(self.new_tags),
    #         *self.feature.get_values(False),
    #         self.poi.type,
    #         self.poi.id,
    #         self.poi.class_,
    #         self.poi.subclass,
    #         self.poi.geom_x,
    #         self.poi.geom_y,
    #         json.dumps(self.poi.tags)
    #     )

    def get_values(self):
        return (
            self.generation_id,
            json.dumps(self.new_tags),
            self.spiders,
            [json.dumps(data) for data in self.properties],
            self.poi.type,
            self.poi.id,
            self.poi.class_,
            self.poi.subclass,
            self.poi.geom_x,
            self.poi.geom_y,
            json.dumps(self.poi.tags)
        )

    # @staticmethod
    # def get_select_fields():
    #     return """
    #     id,
    #     generation_id,
    #     new_tags,
    #     feature_id,
    #     feature_lookup_class,
    #     feature_lookup_subclass,
    #     feature_lookup_name,
    #     feature_lookup_brand,
    #     feature_lookup_operator,
    #     feature_lookup_radius,
    #     ST_X(feature_geom),
    #     ST_Y(feature_geom),
    #     feature_properties,
    #     feature_scraper,
    #     poi_type,
    #     poi_id,
    #     poi_class,
    #     poi_subclass,
    #     ST_X(poi_geom),
    #     ST_Y(poi_geom),
    #     poi_tags
    #     """

    @staticmethod
    def get_select_fields():
        return """
        id,
        generation_id,
        new_tags,
        spiders,
        properties,
        poi_type,
        poi_id,
        poi_class,
        poi_subclass,
        ST_X(poi_geom),
        ST_Y(poi_geom),
        poi_tags
        """

    # @classmethod
    # def from_rs(cls, rs):
    #     return cls(
    #         *rs[0:3],
    #         FeatureWithLookup.from_rs(rs[3:3 + FeatureWithLookup.get_rs_size()]),
    #         PointOfInterest.from_rs(rs[3 + FeatureWithLookup.get_rs_size():])
    #     )

    @classmethod
    def from_rs(cls, rs):
        return cls(
            *rs[0:5],
            PointOfInterest.from_rs(rs[5:])
        )
