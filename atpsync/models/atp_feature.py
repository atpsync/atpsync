from typing import Any


class AtpFeature:
    id: str
    properties: dict[str, Any]
    x: float
    y: float

    @classmethod
    def from_ndgeojson_line(cls, data: any):
        i = cls()
        i.id = data["id"]
        i.properties = data["properties"]
        geometry = data.get("geometry")
        if geometry is None:
            raise ValueError()
        i.x, i.y = geometry["coordinates"]
        return i
