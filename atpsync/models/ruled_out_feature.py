from dataclasses import dataclass

from .feature_with_lookup_definition import FeatureWithLookup
from .poi import PointOfInterest


@dataclass
class RuledOutFeature:
    feature: FeatureWithLookup
    pois: list[PointOfInterest]
