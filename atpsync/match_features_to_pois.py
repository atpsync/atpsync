import time
from itertools import groupby
from pathlib import Path

from atpsync.active_configs import get_active_configs
from atpsync.models import FeatureWithLookup, FoundFeatures, PointOfInterest, RuledOutFeature
from atpsync.utils import mogrify_values


class MatchResult:
    feature: FeatureWithLookup
    poi: PointOfInterest
    distance: float

    @classmethod
    def from_rs(cls, rs):
        i = cls()
        i.feature = FeatureWithLookup.from_rs(rs[0:11])
        i.poi = PointOfInterest.from_rs(rs[11:18])
        i.distance = rs[18]
        return i


def match_features_to_pois(cursor):
    with open(Path(__file__).parent.joinpath("sql/match_features_to_pois.sql"), "r", encoding="utf-8") as f:
        sql = f.read()

    start = time.time()
    cursor.execute(sql)
    all_results = [MatchResult.from_rs(rs) for rs in cursor.fetchall()]
    end = time.time()
    print("execution time", end - start)

    results_within_lookup_radius = [result for result in all_results if
                                    result.distance is not None and result.distance <= result.feature.lookup_radius]

    found_feature_results = []
    ruled_out_features = []
    for _, grouped_results in groupby(results_within_lookup_radius, lambda result: result.feature.id):
        grouped_results = list(grouped_results)
        grouped_results.sort(key=lambda result: result.distance)
        if len(grouped_results) == 1 or (
                len(grouped_results) > 1 and grouped_results[0].distance * 2 <= grouped_results[1].distance):
            found_feature_results.append(grouped_results[0])
        else:
            ruled_out_features.append(
                RuledOutFeature(grouped_results[0].feature, [result.poi for result in grouped_results]))

    cursor.execute("INSERT INTO generations DEFAULT VALUES returning id;")
    generation_id = cursor.fetchone()[0]

    found_features = []
    for key, grouped_found_results in groupby(found_feature_results, lambda result: result.poi.id):
        grouped_found_results = list(grouped_found_results)
        poi = grouped_found_results[0].poi
        new_tags = poi.tags.copy()
        for found_result in grouped_found_results:
            config = list(filter(lambda c: c.spider_name == found_result.feature.spider, get_active_configs()))[0]
            config.apply_feature_to_poi(new_tags, found_result.feature)
        spiders = [found_result.feature.spider for found_result in grouped_found_results]
        properties_list = [found_result.feature.properties for found_result in grouped_found_results]
        found_features.append(FoundFeatures(None, generation_id, new_tags, spiders, properties_list, poi))

    if len(found_features) > 0:
        values = [found_feature.get_values() for found_feature in found_features]
        mogrified_values = mogrify_values(cursor, f"({FoundFeatures.get_template()})", values)
        cursor.execute(
            f"INSERT INTO found_features ({FoundFeatures.get_insert_fields()}) VALUES {mogrified_values}"
        )
