from atpsync.configs.simple_base import SimpleConfig


class LidlCzConfig(SimpleConfig):
    spider_name = "lidl_cz"
    lookup_class = "shop"
    lookup_subclass = "supermarket"
    lookup_name = "%lidl%"
    lookup_brand = "%lidl%"
    lookup_operator = None
    overwrite_tags = {"brand:wikipedia": "cs:Lidl"}
