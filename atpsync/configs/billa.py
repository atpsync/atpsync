from atpsync.configs.simple_base import SimpleConfig


class BillaConfig(SimpleConfig):
    spider_name = "billa"
    lookup_class = "shop"
    lookup_subclass = "supermarket"
    lookup_name = "%billa%"
    lookup_brand = "%billa%"
    lookup_operator = None
