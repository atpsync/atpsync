from typing import Optional

from atpsync.configs.conifg import Config
from atpsync.models import AtpFeature, FeatureWithLookup


class SimpleConfig(Config):
    lookup_class: str
    lookup_subclass: Optional[str]
    lookup_name: Optional[str]
    lookup_brand: Optional[str]
    lookup_operator: Optional[str]
    lookup_radius: int = 100
    overwrite_tags: dict[str, str]

    def map_feature(self, feature: AtpFeature) -> Optional[FeatureWithLookup]:
        return FeatureWithLookup(
            feature.id,
            self.lookup_class,
            self.lookup_subclass,
            self.lookup_name,
            self.lookup_brand,
            self.lookup_operator,
            self.lookup_radius,
            feature.x,
            feature.y,
            feature.properties,
            self.spider_name
        )

    def apply_feature_to_poi(self, tags: dict[str, str], feature: FeatureWithLookup):
        copy_tags_from_feature = ["name", "brand", "brand:wikidata", "located_in"]

        # remove 'source:' tags for overwritten tags
        for copied_tag in copy_tags_from_feature:
            if copied_tag.startswith("source:"):
                continue
            # e.g. brand:wikidata -> source:brand
            tag_root = copied_tag.split(":")[0]
            source_prefix = f"source:{tag_root}"
            for tag in list(tags.keys()):
                if tag.startswith(source_prefix):
                    tags.pop(tag)

        for tag in copy_tags_from_feature:
            if value := feature.properties.get(tag):
                tags[tag] = str(value)

        if hasattr(self, "overwrite_tags"):
            for tag, value in self.overwrite_tags.items():
                tags[tag] = value
