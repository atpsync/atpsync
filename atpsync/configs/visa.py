from dataclasses import dataclass
from typing import Optional

from atpsync.configs.conifg import Config
from atpsync.models import FeatureWithLookup, AtpFeature


@dataclass
class VisaOperator:
    name: str
    wikidata: str
    wikipedia: Optional[str]


class VisaConfig(Config):
    spider_name = "visa"
    operator_map = {
        "CESKA SPORITELNA, A.S.": VisaOperator("Česká spořitelna", "Q341100", "cs:Česká spořitelna")
    }

    def map_feature(self, feature: AtpFeature) -> Optional[FeatureWithLookup]:
        osm_operator = self.operator_map.get(feature.properties.get("operator"))
        if osm_operator is None:
            return None

        return FeatureWithLookup(
            feature.id,
            "amenity",
            "atm",
            None,
            None,
            osm_operator.name,
            10,
            feature.x,
            feature.y,
            feature.properties,
            self.spider_name
        )

    def apply_feature_to_poi(self, tags: dict[str, str], feature: FeatureWithLookup):
        atp_operator = feature.properties["operator"]
        visa_operator = self.operator_map[atp_operator]
        tags["brand"] = visa_operator.name
        tags["operator"] = visa_operator.name

        tags["brand:wikidata"] = visa_operator.wikidata
        tags["operator:wikidata"] = visa_operator.wikidata

        if wikipedia := visa_operator.wikipedia:
            tags["brand:wikipedia"] = wikipedia
            tags["operator:wikipedia"] = wikipedia
