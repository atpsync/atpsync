from atpsync.models import AtpFeature, FeatureWithLookup


class Config:
    spider_name: str

    def map_feature(self, feature: AtpFeature) -> FeatureWithLookup:
        raise NotImplementedError()

    def apply_feature_to_poi(self, tags: dict[str, str], feature: FeatureWithLookup):
        raise NotImplementedError()
