from atpsync.configs.simple_base import SimpleConfig


class McDonaldsCzConfig(SimpleConfig):
    spider_name = "mcdonalds_cz"
    lookup_class = "amenity"
    lookup_subclass = "fast_food"
    lookup_name = "%donald%"
    lookup_brand = "%donald%"
    lookup_operator = None
