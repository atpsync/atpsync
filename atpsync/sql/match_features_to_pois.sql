
SELECT
    features.id,
    features.lookup_class,
    features.lookup_subclass,
    features.lookup_name,
    features.lookup_brand,
    features.lookup_operator,
    features.lookup_radius,
    ST_X(features.geom),
    ST_Y(features.geom),
    features.properties,
    features.spider,
    pois.type,
    pois.id,
    pois.class,
    pois.subclass,
    ST_X(ST_Transform(pois.geom, 4326)),
    ST_Y(ST_Transform(pois.geom, 4326)),
    pois.tags,
    ST_DistanceSphere(ST_Transform(pois.geom, 4326), features.geom) AS distance
FROM features
LEFT JOIN pois on
    -- lookup POIs based on their brand:wikidata and ref tags
    (
        features.properties_brand_wikidata = pois.tags_brand_wikidata
    AND features.properties_ref = pois.tags_ref
    )
OR  -- features nearby
    (
        features.lookup_class = pois.class
    AND (CASE WHEN features.lookup_subclass IS null THEN true ELSE features.lookup_subclass = pois.subclass END)
    AND ST_DistanceSphere(features.geom, ST_Transform(pois.geom, 4326)) <= features.lookup_radius * 2
    AND (
            (CASE WHEN features.lookup_name IS null THEN false ELSE pois.tags_name ILIKE features.lookup_name END)
        OR  (CASE WHEN features.lookup_brand IS null THEN false ELSE pois.tags_brand ILIKE features.lookup_brand END)
        OR  (CASE WHEN features.lookup_operator IS null THEN false ELSE pois.tags_operator ILIKE features.lookup_operator END)
        )
    );
