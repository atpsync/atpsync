from atpsync.configs.billa import BillaConfig
from atpsync.configs.lidl_cz import LidlCzConfig
from atpsync.configs.mcdonalds_cz import McDonaldsCzConfig
from atpsync.configs.visa import VisaConfig


def get_active_configs():
    return [LidlCzConfig()]
    # return [BillaConfig(), LidlCzConfig(), McDonaldsCzConfig(), VisaConfig()]
