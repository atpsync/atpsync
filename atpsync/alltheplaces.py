import json
import os
import subprocess
from pathlib import Path
from typing import Iterator

from atpsync.models import AtpFeature


def git_clone(repo: str, path: Path):
    subprocess.check_call(["git", "clone", "--depth=1", repo, str(path.absolute())])


def git_pull(path: Path):
    subprocess.check_call(["git", "pull"], cwd=path)


def pipenv_sync(path: Path):
    env = os.environ.copy()
    env["PIPENV_VERBOSITY"] = "-1"
    subprocess.check_call(["pipenv", "sync"], cwd=path, env=env)


def pipenv_run_popen(args, **call_kwargs):
    env = call_kwargs.setdefault("env", os.environ.copy())
    env["PIPENV_VERBOSITY"] = "-1"
    return subprocess.Popen(["pipenv", "run", *args], **call_kwargs)


class AllThePlacesManager:
    path: Path

    def __init__(self, path: Path):
        self.path = path

    def update(self):
        if self.path.exists():
            self._pull_latest()
        else:
            self._clone_latest()
        pipenv_sync(self.path)

    def _clone_latest(self):
        git_clone("https://github.com/alltheplaces/alltheplaces.git", self.path)

    def _pull_latest(self):
        git_pull(self.path)

    def run_spider(self, name: str) -> Iterator[AtpFeature]:
        process = pipenv_run_popen(
            ["scrapy", "runspider", f"locations/spiders/{name}.py", "--nolog", "-o", "-:ndgeojson"],
            cwd=self.path,
            stdout=subprocess.PIPE
        )
        while line := process.stdout.readline():
            data = json.loads(line)
            try:
                feature = AtpFeature.from_ndgeojson_line(data)
            except ValueError:
                continue
            yield feature
