import json_stream
from pathlib import Path

import atpsync.models.atp_feature

from atpsync.alltheplaces import AllThePlacesManager
from atpsync.active_configs import get_active_configs
from atpsync.models import FeatureWithLookup
from atpsync.utils import mogrify_values


def get_debug_spider_data(path: Path):
    with open(path, "r", encoding="utf-8") as file:
        for feature_json in json_stream.load(file)["features"]:
            try:
                atp_feature = atpsync.models.atp_feature.AtpFeature.from_ndgeojson_line(
                    json_stream.to_standard_types(feature_json))
            except ValueError:
                continue
            yield atp_feature


def update_features(cursor, permanent_cache_path: Path, debug_files_path: Path):
    atp = AllThePlacesManager(permanent_cache_path.joinpath("alltheplaces"))
    atp.update()

    cursor.execute("TRUNCATE features")

    for config in get_active_configs():

        debug_spider_data_path = debug_files_path.joinpath(f"spiders/{config.spider_name}.geojson")
        if debug_spider_data_path.exists():
            print(f"using debug data for spider '{config.spider_name}'")
            atp_features = get_debug_spider_data(debug_spider_data_path)
        else:
            print(f"fetching ATP data for spider '{config.spider_name}'")
            atp_features = atp.run_spider(config.spider_name)

        features = []
        for atp_feature in atp_features:
            if feature := config.map_feature(atp_feature):
                features.append(feature)
                if len(features) > 500:
                    insert_features(cursor, features)
                    features.clear()
        insert_features(cursor, features)


def insert_features(cursor, features):
    if len(features) == 0:
        return
    cursor.execute(
        f"INSERT INTO features VALUES " + \
        mogrify_values(
            cursor,
            f"({FeatureWithLookup.get_template(True)})",
            [feature.get_values(True) for feature in features]
        ),
    )
