def mogrify_values(db_cursor, template, values):
    return ','.join([
        db_cursor.mogrify(template, value).decode() for value in values
    ])
