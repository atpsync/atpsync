# atpsync


## Development

The following graph sums up the `atpsync` architecture:

```
     OpenStreetMap         All The Places  
           |                     |         
           v                     v         
   config.get_filter     config.map_feature
           |                     |         
           +---------+   +-------+         
                     |   |                 
        +------------+---+------------+    
PostGIS |            v   v            |    
        |     Processing database     |    
        |              |              |    
        |              v              |    
        | config.apply_feature_to_poi |    
        |              |              |    
        |              v              |    
        |       Output database       |    
        |          |   |   |          |    
        +----------+---+---+----------+    
                   |   |   |               
            +------+   |   +------+        
            v          v          v        
       Statistics    SCEE     Update OSM   
```

### OSM Import

Currently to import OSM data run:
```bash
osm2pgsql -d "<postgres conninfo>" -O flex -S osm2pgsql.lua "<.osm pbf file path>"
```

In future the Planet.osm diffs will be automatically imported instead.
