CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS pg_trgm;


CREATE TABLE features
(
    id                        varchar primary key,
    lookup_class              text                  not null,
    lookup_subclass           text,
    lookup_name               text,
    lookup_brand              text,
    lookup_operator           text,
    lookup_radius             int                   not null,
    geom                      geometry(POINT, 4326) not null,
    properties                jsonb                 not null,
    spider                    text                  not null,
    -- jsonb lookup cache
    properties_brand_wikidata text,
    properties_ref            text
);

CREATE TABLE generations
(
    id serial primary key
);

CREATE TABLE found_features
(
    id            serial primary key,
    generation_id bigint                not null,
    new_tags      jsonb                 not null,

    spiders       text[]                not null,
    properties    jsonb[]               not null,

--     -- 'feature_' from table 'features'
--     feature_id                        varchar               not null,
--     feature_lookup_class              text                  not null,
--     feature_lookup_subclass           text,
--     feature_lookup_name               text,
--     feature_lookup_brand              text,
--     feature_lookup_operator           text,
--     feature_lookup_radius             int                   not null,
--     feature_geom                      geometry(POINT, 4326) not null,
--     feature_properties                jsonb                 not null,
--     feature_scraper                   text                  not null,
--
--     feature_properties_brand_wikidata text,
--     feature_properties_ref            text,

    -- 'poi_' from table 'pois'
    poi_type      char                  not null,
    poi_id        bigint                not null,
    poi_class     text                  not null,
    poi_subclass  text                  not null,
    poi_geom      geometry(POINT, 4326) not null,
    poi_tags      jsonb                 not null,

    CONSTRAINT fk_generation
        FOREIGN KEY (generation_id)
            REFERENCES generations (id)
);
