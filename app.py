import json
from pathlib import Path

from flask import Flask, Response, send_file

from atpsync.models import FoundFeatures
from postgres_db import PostgresDB

app = Flask(__name__)

db = PostgresDB()


@app.route("/")
def index():
    return send_file(Path(__file__).parent.joinpath("static/index.html"))


TYPE_MAP = {
    "N": "node",
    "W": "way"
}


@app.route("/api/1/map/data")
def get_map_data():
    with db.get_cursor() as cursor:
        cursor.execute(
            f"SELECT {FoundFeatures.get_select_fields()} FROM found_features"
        )
        found_features = [FoundFeatures.from_rs(rs) for rs in cursor.fetchall()]

    data = {
        "configurations": [],
        "not_found": [],
        "ruled_out": [],
        "found": [{
            "configuration_names": found_feature.spiders,
            "element": {
                "id": found_feature.poi.id,
                "type": TYPE_MAP[found_feature.poi.type],
                "lat": found_feature.poi.geom_y,
                "lon": found_feature.poi.geom_x,
                "tags": found_feature.poi.tags
            },
            "new_tags": found_feature.new_tags,
        } for found_feature in found_features],
    }

    return Response(json.dumps(data), content_type="application/json")


if __name__ == "__main__":
    app.run()
