const map = L.map('map').setView([49.820, 15.475], 8);

L.tileLayer('https://tile-{s}.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/**
 * @param {string} unsafe
 * @returns {string}
 */
function escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

async function init() {
    let response = await fetch("/api/1/map/data");
    let {configurations, not_found, ruled_out, found} = await response.json();

    add_configurations(configurations);

    const stats_el = document.querySelector("#stats");
    stats_el.innerHTML = `
    <p id="stats_not_found">nenalezeno: ${escapeHtml(not_found.length.toString())}</p>
    <p id="stats_ruled_out">vyřazeno: ${escapeHtml(ruled_out.length.toString())}</p>
    <p id="stats_found">nalezeno: ${escapeHtml(found.length.toString())}</p>
    `;

    for (const entry of not_found) {
        add_not_found(entry);
    }

    for (const entry of ruled_out) {
        add_ruled_out(entry);
    }

    for (const entry of found) {
        add_found(entry);
    }
}

function add_configurations(configurations) {
    let html = "";

    for (const configuration of configurations) {
        const url = `https://gitlab.com/vfosnar/atpsync/-/blob/main/atpsync/configurations/${configuration.name}.py`;
        const spider_url = `https://github.com/alltheplaces/alltheplaces/blob/master/locations/spiders/${configuration.spider_name}.py`;
        html += `
            <li>
                <a target="_blank" href="${escapeHtml(spider_url)}">${configuration.spider_name}</a>
                (<a target="_blank" href="${escapeHtml(url)}">config</a>)
            </li>`;
    }

    const configurations_el = document.querySelector("#configurations");
    configurations_el.innerHTML = html;
}

/**
 * @param {number} num
 * @returns {string}
 */
function round(num) {
    return num.toLocaleString(undefined, {maximumFractionDigits: 2, minimumFractionDigits: 2});
}

/**
 * @param {Array<string>} configuration_names
 * @returns {string}
 */
function format_configuration_names(configuration_names) {
    return configuration_names.join(", ");
}

/**
 * @param {Object.<string, string>} tags
 * @returns {string}
 */
function render_tags_table(tags) {
    let html = "<h3>Tagy</h3>";
    html += "<table><thead><th>Tag</th><th>Hodnota</th></thead><tbody>";
    for (const [key, value] of Object.entries(tags)) {
        html += `<tr><th>${escapeHtml(key)}</th><td>${escapeHtml(value)}</td></tr>`;
    }
    html += "</tbody></table>";
    return html;
}

/**
 * @param {Object.<string, string>} original_tags
 * @param {Object.<string, string>} new_tags
 * @returns {string}
 */
function render_tags_difference_table(original_tags, new_tags) {
    let html = "<h3>Tagy</h3>";
    html += `
        <table><thead>
            <th>Tag</th>
            <th>Původní</th>
            <th>Nová</th>
        </thead><tbody>`;
    for (const [key, new_value] of Object.entries(new_tags)) {
        let original_value = "";
        if (Object.keys(original_tags).includes(key)) {
            original_value = original_tags[key];
        }
        const rowClass = original_value === new_value ? "" : "changed-tag";
        html += `
            <tr class="${rowClass}">
                <th><span>${escapeHtml(key)}</span></th>
                <td><span>${escapeHtml(original_value)}</span></td>
                <td><span>${escapeHtml(new_value)}</span></td>
            </tr>`;
    }
    for (const [key, original_value] of Object.entries(original_tags)) {
        if (!Object.keys(new_tags).includes(key)) {
            console.log(key, original_value);
            html += `
                <tr class="removed-tag">
                    <th><span>${escapeHtml(key)}</span></th>
                    <td><span>${escapeHtml(original_value)}</span></td>
                    <td></td>
                </tr>`;
        }
    }
    html += "</tbody></table>";
    return html;
}

function add_not_found(entry) {
    const feature = entry.feature;

    let html = "";

    html += `<h2>${format_configuration_names(entry.configuration_names)}</h2>`;
    if (entry.precise_enough_for_hint)
        html += `<p><a target="_blank" href="https://www.openstreetmap.org/query?lat=${feature.lat}&lon=${feature.lon}#map=19/${feature.lat}/${feature.lon}">Prohledat OSM</a></p>`
    else {
        html += `<p><b>Lokace nebývá přesná, např. pouze na úrovni města</b></p>`
        html += `<p><a target="_blank" href="https://www.openstreetmap.org/#map=14/${feature.lat}/${feature.lon}">Zobrazit okolí na OSM</a></p>`
        html += "<p>Pro spojení s OSM prvkem mu přidejte:</p>"
        html += "<table><tbody>"
        const refs = Object.keys(feature.tags).filter(tag => tag.startsWith("ref:"));
        for (const key of refs.concat(["brand:wikidata"])) {
            html += `<tr><td>${escapeHtml(key)}</td><td>${escapeHtml(feature.tags[key])}</td></tr>`
        }
        html += "</tbody></table>"
    }

    html += render_tags_table(entry.feature.tags);

    L.circle([feature.lat, feature.lon], {
        color: 'red',
        fillColor: 'red',
        fillOpacity: 0.5,
        radius: 100
    })
        .addTo(map)
        .bindPopup(html, {maxWidth: 500});
}

function add_ruled_out(entry) {
    const element = entry.element;

    let html = "";

    html += `<h2>${format_configuration_names(entry.configuration_names)}</h2>`;
    html += `<p>prvek: <a target="_blank" href="https://www.openstreetmap.org/${element.type}/${element.id}">${element.type}/${element.id}</a></p>`;

    switch (entry.reason.type) {
        case "nearby_branch":
            html += `<p>důvod vyřazení: blízkost jiné lokace <a target="_blank" href="https://www.openstreetmap.org/${entry.reason.data.type}/${entry.reason.data.id}">${entry.reason.data.type}/${entry.reason.data.id}</a></p>`;
            break;

        default:
            html += `<p>neznámý důvod vyřazení: ${entry.reason.type}</p>`;
            break;
    }

    html += render_tags_difference_table(element.tags, entry.new_tags);

    L.circle([element.lat, element.lon], {
        color: 'orange',
        fillColor: 'orange',
        fillOpacity: 0.5,
        radius: 100
    })
        .addTo(map)
        .bindPopup(html, {maxWidth: 500});

    if (entry.reason.type === "nearby_branch") {
        var coords = [
            L.latLng(element.lat, element.lon),
            L.latLng(entry.reason.data.lat, entry.reason.data.lon)
        ];
        L.polyline(coords, {color: 'orange', weight: 5}).addTo(map);
    }
}

function add_found(entry) {
    const element = entry.element;

    let html = "";

    html += `<h2>${format_configuration_names(entry.configuration_names)}</h2>`;
    html += `<p>prvek: <a target="_blank" href="https://www.openstreetmap.org/${element.type}/${element.id}">${element.type}/${element.id}</a></p>`;

    html += render_tags_difference_table(element.tags, entry.new_tags);

    L.circle([element.lat, element.lon], {
        color: 'green',
        fillColor: 'green',
        fillOpacity: 0.5,
        radius: 100
    })
        .addTo(map)
        .bindPopup(html, {maxWidth: 500});
}

init();
